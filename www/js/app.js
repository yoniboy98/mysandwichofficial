var $ = Dom7;

var device = Framework7.getDevice();
var app = new Framework7({
  name: 'mySandwich', // App name
  theme: 'auto', // Automatic theme detection
  el: '#app', // App root element

  id: 'be.myhostingname.mysandwich', // App bundle ID
  // App store
  store: store,
  // App routes
  routes: routes,


  // Input settings
  input: {
    scrollIntoViewOnFocus: device.cordova && !device.electron,
    scrollIntoViewCentered: device.cordova && !device.electron,
  },
  // Cordova Statusbar settings
  statusbar: {
    iosOverlaysWebView: true,
    androidOverlaysWebView: false,
  },
  on: {
    init: function () {
      var f7 = this;
      if (f7.device.cordova) {
        // Init cordova APIs (see cordova-app.js)
        cordovaApp.init(f7);
        onDeviceReady();
      
      }
    },
    pageInit: function(page) {
      // andere methode om functies aan een knop te hangen
      if (page.route.name === "index") {
          signInUi();
          setTimeout(getTime, 1000);
          setTimeout(getSandwiches, 800);
          getSandwichesForUpdate();
          setTimeout(getAllOrders, 2000);
         setTimeout(getAllOrdershistory, 2000);
          setTimeout(getUserOrdersOnId, 2000);
          setTimeout(getUserInformation, 2000);
          setTimeout(getSandwichesForDelete, 2500);
   
    
          $('#ToggleButton').on('click', function () {
              ToggleThemeButton();
          });

          $('#btnAddSandwich').on('click', function () {
          addSandwich();
          });

          $('#btnUpdateSandwich').on('click', function () {
            app.dialog.confirm("Are you sure?", function(){
            updateSandwiches();
          });
            });

              $('#LoadMoreOrderListBtn').on('click', function () {
                getOrdersAllOnLoadMore();
                $("#LoadMoreOrderListBtn").remove();
                });

                $('#LoadMoreHistoryBtn').on('click', function () {
                  getHistoryOrdersAllOnLoadMore();
                  $("#LoadMoreHistoryBtn").remove();
                  });

                  $('#LoadMoreBtn').on('click', function () {
                    getUserOrdersOnIdAll();
                    $("#LoadMoreBtn").remove();
                    });
        
      }
      
    }
  },
  
});




// functions 

// vraagt de tijd op van firestore
 function  getTime(){
   
  var today =  firebase.firestore.Timestamp.now().toDate()


today =  today.getHours() + ":" + today.getMinutes()

if(today >= '18:00:00'){
  $("#btnOrder").remove();
  document.getElementById("time").innerHTML+=`<div>We Are Closed ! :( </div>`;

}
else if(today < '08:00:00') {
  $("#btnOrder").remove();
  document.getElementById("time").innerHTML+=`<div>We Are Closed ! :(</div>`;
  
}
else if(today >= '08:00:00') {
  document.getElementById("time").innerHTML+=`<div>We Are Open ! :) </div>`;

}

}




// theme switch 
    function ToggleThemeButton() {

      if($('#ToggleButton').is(':checked')){
        $('#toggleMe').removeClass('theme-light')
        .addClass('theme-dark');
        $('#toggleOrdersOnIdPage').removeClass('theme-dark')
        .addClass('theme-light');
        $('#toggleUserInformation').removeClass('theme-dark')
        .addClass('theme-light');
        $('#toggleMeTo').removeClass('theme-dark')
        .addClass('theme-light');
        $('#toggleDeleteSandwiches').removeClass('theme-dark')
        .addClass('theme-light');
        $('#toggleAdmin').removeClass('theme-dark')
        .addClass('theme-light');
        $('#toggleAdd').removeClass('theme-dark')
        .addClass('theme-light');
        $('#toggleUpdate').removeClass('theme-dark')
        .addClass('theme-light');
        $('#toggleOrdersPage').removeClass('theme-dark')
        .addClass('theme-light');
       
      } else {
        $('#toggleMe').removeClass('theme-dark')
        .addClass('theme-light');
        $('#toggleOrdersOnIdPage').removeClass('theme-light')
        .addClass('theme-dark');
        $('#toggleMeTo').removeClass('theme-light')
        .addClass('theme-dark');
        $('#toggleUserInformation').removeClass('theme-light')
        .addClass('theme-dark');
        $('#toggleDeleteSandwiches').removeClass('theme-light')
        .addClass('theme-dark');
        $('#toggleAdmin').removeClass('theme-light')
        .addClass('theme-dark');
        $('#toggleAdd').removeClass('theme-light')
        .addClass('theme-dark');
        $('#toggleUpdate').removeClass('theme-light')
        .addClass('theme-dark');
        $('#toggleOrdersPage').removeClass('theme-light')
        .addClass('theme-dark');
      }
    }
 


// popup dat de sign in weergeeft.
function signInCheck() {
  app.popup.open("#login-popup");

}


function signInUi() {

 // Initialize the FirebaseUI Widget using Firebase.
 let ui = firebaseui.auth.AuthUI.getInstance()
 || new firebaseui.auth.AuthUI(firebase.auth());
  
  var uiConfig = {
    callbacks: {
      signInSuccessWithAuthResult: function() {
        // User successfully signed in.
        // Return type determines whether we continue the redirect automatically
        // or whether we leave that to developer to handle.

          app.popup.close();
        
        return;
      },

    },
    // Will use popup for IDP Providers sign-in flow instead of the default, redirect.
    signInFlow: 'redirect',
    signInOptions: [
      // Leave the lines as is for the providers you want to offer your users.
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,

    ],
  
  };
  // The start method will wait until the DOM is loaded.
  ui.start('#firebaseui-auth-container', uiConfig);



}


// broodjes weergeven
function getSandwiches() {


db.collection("sandwiches").onSnapshot(function(snapshot){
  document.getElementById("SandwichList").innerHTML='';
  let id = 1

  snapshot.forEach(function(sandwich) {

    let name = sandwich.data().sandwichName
    let price = sandwich.data().sandwichPrice
    let available = sandwich.data().available
   

    document.getElementById("SandwichList").innerHTML+=`<div class="page-content">`+
    `<div class="card demo-facebook-card" id="sandwichCard${id} ">`+
      `<div class="card-header">`+
        `<div class="demo-facebook-name"> <p id="orderName">${name}</p></div> `+
        `<div class="stepper stepper-init">`+
        `<p id="quantityP">Quantity </p> `+
        ` <div class="stepper-input-wrap">`+
        ` <input type="number" value="1" min="0" max="100" step="1" id="orderCount" />`+
        ` </div>`+
        ` </div>`+
          `<div class="demo-facebook-date" ><div>Euro per item</div><p id="orderPrice">€ ${price}</p></div></div>`+
          `<div class="card-content"> <img src="${sandwich.data().imageUrl}" id="imageId" /></div>`+
          `<div class="card-footer"><a href="#" class="link">`+
            ` <select id="orderExtra" class="form-control" type="text"> `+
              ` <option value="no extra">Choose extra... </option> `+
              ` <option value="sla, tomaat, gehakt">sla, tomaat, gehakt</option>`+
              ` <option value="hesp, kaas, zonder tomaat ">hesp, kaas, zonder tomaat </option>`+
              ` <option value="sla">sla </option>`+
              ` <option value="tomaten en komkommers">tomaten en komkommers</option>`+
              ` </select>`+
              `     </a><a href="#" class="link">`+
                `<select id="orderSauce" class="form-control" type="text">`+
                  `<option value="no sauce">Choose sauce... </option>`+
                  `<option value="mayonaise">mayonaise</option>`+
                  `<option value="bbq">bbq</option>`+
                  `<option value="ketchup">ketchup</option>`+
                  `<option value="joppie">joppie</option>`+
                  `</select>`+
                  ` </a><a href="#"class="link" id="orderAvailable"><p>${available}</p></a></div>`+
            `<div class="button" id="btnOrder"><button onClick=OrderSandwich(this.parentNode.parentNode); class='button button-fill button-raised button-small` +
            `color-orange col'>Order </button></div>`+
            `</div></div>`
          
 
       id++;
  })
 
})

}



// broodjes weergeven om te verwijderen
function getSandwichesForDelete() {


  db.collection("sandwiches").onSnapshot(function(snapshot){
    document.getElementById("SandwichDelete").innerHTML='';
    snapshot.forEach(function(sandwich) {
  
      document.getElementById("SandwichDelete").innerHTML+=`<div class="page-content">`+
      `<div class="card demo-facebook-card">`+
        `<div class="card-header">`+
          `<div class="demo-facebook-name">${sandwich.data().sandwichName}</p></div>`+
            `<div class="demo-facebook-date" ><div>Euro per item</div><p id="orderPrice"> ${sandwich.data().sandwichPrice}</p></div></div>`+
            `<div class="card-content"> <img src="${sandwich.data().imageUrl}" id="imageId" /></div>`+
            `<div class="card-footer"><a href="#" class="link">`+
              ` <select  class="form-control" type="text"> `+
                ` <option value="no extra">Choose extra... </option> `+
                ` <option value="sla, tomaat, gehakt">sla, tomaat, gehakt</option>`+
                ` <option value="hesp, kaas, zonder tomaat ">hesp, kaas, zonder tomaat </option>`+
                ` <option value="sla">sla </option>`+
                ` <option value="tomaten en komkommers">tomaten en komkommers</option>`+
                ` </select>`+
                `     </a><a href="#" class="link">`+
                  `<select  class="form-control" type="text">`+
                    `<option value="no sauce">Choose sauce... </option>`+
                    `<option value="mayonaise">mayonaise</option>`+
                    `<option value="bbq">bbq</option>`+
                    `<option value="ketchup">ketchup</option>`+
                    `<option value="joppie">joppie</option>`+
                    `</select>`+
                    ` </a><a href="#"class="link"><p>${sandwich.data().available}</p></a></div>`+
            `<div class="button" id="delBtn"><button  onClick=deleteSandwichOnId('${sandwich.id}'); class='button button-fill button-raised button-small` +
              `color-orange col'>Delete</button></div>`+ 
              `</div></div>`
            
   
         
    })
   
  })
  
  }



// een lijst van de broodjes, deze zijn bedoeld op te selecteren voor update
function getSandwichesForUpdate(){
 db.collection("sandwiches").onSnapshot(function(snapshot){
    document.getElementById("updateList").innerHTML='';
    snapshot.forEach(function(sandwich) {


      let sandwichData = sandwich.data();


      
      document.getElementById("updateList").innerHTML+= 
      `<div class="list">`+
      `<ul><li>`+
      ` <a href="#" class="item-link item-content">Name: ${sandwichData.sandwichName}`+
      `  <div class="item-media"><i class="icon icon-f7"></i></div>`+
      ` <div class="item-inner">`+
      `<div class="item-title">${sandwichData.sandwichPrice} euro`+
      `<div class="item-header">${sandwichData.available}</div>`+
      ` </div>`+
      ` <div class="item-after"> <div onClick="setUpdateValueInFields('${sandwich.id}')">Edit</div></div>`+
      ` </div> </a></li>`
    })
  })

}

// set value in input fields
function setUpdateValueInFields(sandWichID){
  
  db.collection("sandwiches").doc(sandWichID)
  .get()
  .then((doc) => {
        if (doc.exists) {
              //    console.log("Document data:", doc.data());
                 
                  $("#sandwichname").val(doc.data().sandwichName)
                  $("#sandwichprice").val(doc.data().sandwichPrice)
                  $("#available").val(doc.data().available)
                
                  
                  }
       else {        
         // doc.data() will be undefined in this case
                console.log("No such document!"); 
         }}).catch((error) => { 
              console.log("Error getting document:", error);
            
            });

            if(document.getElementById("btnUpdateSandwich") == null){
            document.getElementById("updateBtns").innerHTML +=  `<a href="#" id="btnUpdateSandwich" onclick="updateSandwiches('${sandWichID}')" class="button button-fill button-raised">Update Snack</a>`
            } else {
              document.getElementById("btnUpdateSandwich").remove();
              document.getElementById("updateBtns").innerHTML +=  `<a href="#" id="btnUpdateSandwich" onclick="updateSandwiches('${sandWichID}')" class="button button-fill button-raised">Update Snack</a>`

              
            }
}


// deze functie update de broodjes op id
function updateSandwiches(id){

      
  
    app.dialog.confirm("Are you sure?", function(){
     
  let sandwichname = document.getElementById("sandwichname").value;
  let sandwichprice = document.getElementById("sandwichprice").value;
  let available = document.getElementById("available").value;


 
  var dbInput = db.collection("sandwiches").doc(id);
  

 
// update document
  return dbInput.update({
    sandwichName: sandwichname,
    sandwichPrice: sandwichprice,
    sandwichExtra: "no-extra",
    sandwichSauce: "no-sauce",
    available: available
  })
  .then(() => {
     app.dialog.alert("Sandwich successfully updated!");
  })
  .catch((error) => {
      // The document probably doesn't exist.
      app.dialog.alert("This document doesn't excist ! ");
  });

  });
  


}


    // clear function
    function clearAddSandwichForm() {
      $("#sandwichname").val("")
      $("#sandwichprice").val("")
      $("#mediaCapture").val("")
    }

    // zet var storageRef = firebase.storage().ref('images/'+ imageName) op een andere locatie zodat die 1st uitgevoerd kan worden voor wnr addsandwich uitgevoerd wordt
  function addSandwich() {

let sandwichname = document.getElementById("sandwichname").value;
let sandwichprice = document.getElementById("sandwichprice").value;
let available = document.getElementById("available").value;
var input = document.getElementById("mediaCapture").files[0];

var imageName = input.name;

var storageRef = firebase.storage().ref('images/'+ imageName)

 storageRef.put(input);
 console.log("wait !")

firebase.storage().ref("images/" + input.name ).getDownloadURL().then(function(url) {
  
  

  var sandwich = {
    sandwichName: sandwichname,
    sandwichPrice: sandwichprice,
    sandwichExtra: "no-extra",
    sandwichSauce: "no-sauce",
    available: available,
    imageUrl: url
  }

    // validation
    if ($("#sandwichname").hasClass("input-invalid") || $("#sandwichname").val() == "" ) {
      app.dialog.alert("Make sure your name is valid !");
    } else if ($("#sandwichprice").hasClass("input-invalid") || $("#sandwichprice").val() == "" ) {
      app.dialog.alert("Make sure you enter a correct price for your sandwich !");
    }  else {
      // input succes validation

  // Add a new document with a generated id.
  let dbAdd = db.collection("sandwiches");
  dbAdd.add(sandwich).then(() =>{
    app.dialog.alert("sandwich has been successfully added!");
  clearAddSandwichForm();
  })
    }
  }).catch(function(error) {
    app.dialog.alert("image is added to the database storage, wait 2 sec and try again ! ");
  });

    }





// delete sandwich on id
    function deleteSandwichOnId(id) {
      app.dialog.confirm("Are you sure?", function(){
db.collection("sandwiches").doc(id).delete().then(()=>{
  app.dialog.alert("sandwich has been successfully deleted!");
}) .catch(function(){
  app.dialog.alert("Something went wrong :(");
});
});
    }


    // order een broodje met alle bijhoorende informatie
function OrderSandwich(card) {


  // notification in app
var notificationFull = app.notification.create({
  title: 'Wait until the green address is visible on your order.' ,
  subtitle: 'You can view your order in My Orders !',
  text: '(Tap to close)',
  closeOnClick: true,

});

  let aantal = card.parentNode.children[0].children[0].children[1].children[1].children[0].value
  let sauce = card.parentNode.children[0].children[2].children[1].children[0].value
  let extra = card.parentNode.children[0].children[2].children[0].children[0].value
  let naam = card.parentNode.children[0].children[0].children[0].children[0].textContent
  let available = card.parentNode.children[0].children[2].children[2].children[0].textContent
  let prijs = card.parentNode.children[0].children[0].children[2].children[1].textContent
 
let subStringPrijs = prijs.substring(2,7);

if(aantal <= 0){
  app.dialog.alert("You have not entered a valid quantity !");
} else if (available == "not available") {
  app.dialog.alert("This sandwich is currently not available !");
} else if (firebase.auth().currentUser.uid == "") {
  app.dialog.alert("Close this app now !");
}
else {
  let orderTotal = subStringPrijs * aantal

  var order = {
        quantity: aantal,
        ordername: naam,
        orderextra: extra,
        orderprice: prijs,
        orderavailable: available,
        ordersauce: sauce,
        completed: false,
        customer: firebase.auth().currentUser.displayName,
        customerMail: firebase.auth().currentUser.email,
        customerID: firebase.auth().currentUser.uid,
        orderTotalAmmountInEuro: orderTotal,
        time: firebase.firestore.Timestamp.now().toDate()
      
      
  }
  
  // Add a new document with a generated id.
  let dbOrder = db.collection("orders");
  app.dialog.confirm("Are you sure that you want "+ aantal +" of "+ naam + " for " + orderTotal + " euro ?", function(){
  dbOrder.add(order).then(() =>{
    app.dialog.alert(aantal +" of "+ naam +" has been successfully ordered!");
    notificationFull.open()
  });
});
}
}
 
// geeft alle orders weer in firestore.
function getAllOrders() {

   db.collection("orders").where('completed', '==', false).orderBy("time").limit(10).onSnapshot(function(snapshot){
    document.getElementById("orderList").innerHTML='';
    document.getElementById("orderBadgeAdminCount").innerHTML= snapshot.size;
    snapshot.forEach(function(orders) {
    //  console.log(orders.data())
  
      document.getElementById("orderList").innerHTML+=
      `<div class="card">`+
        `<div class="card-header">Sandwich: ${orders.data().ordername}</div>`+
        `<div class="card-content">`+
          ` <div class="list media-list no-ios-edges">`+
            `<ul>`+
              `<li class="item-content">`+
                `<div class="item-inner">`+
                    ` <div class="item-subtitle">ready: ${orders.data().completed}</div>`+
                    `</div>`+
                    ` </li>`+
                    ` <div class="item-content">${orders.data().orderavailable}</div>`+
                    ` <div class="item-content">Quantity: ${orders.data().quantity}</div>`+
                    ` <div class="item-content">Sauce: ${orders.data().ordersauce}</div>`+
                    `<div class="item-content">Extra's: ${orders.data().orderextra}</div>`+
                    `<div class="item-content">Customer: ${orders.data().customer}</div>`+
                    `<div class="item-content">Customer Email: ${orders.data().customerMail}</div>`+
                    ` </ul> </div></div>`+
                    `  <div class="card-footer"> <span>Date: ${orders.data().time.toDate()}</span><span>${orders.data().orderTotalAmmountInEuro} euro</span></div>`+
                    `<div class="button"><button onClick=roundSandwich('${orders.id}'); class='button button-fill button-raised button-small` +
                    `color-orange col'>Is Ready </button></div>`+
                    ` </div>`

    })
  })
  
}




// een history van alle bestelde broodjes.
function getAllOrdershistory() {

db.collection("orders").where('completed', '==', true).orderBy("time", "desc").limit(10).onSnapshot(function(snapshot){
    document.getElementById("historyOrderList").innerHTML='';
    snapshot.forEach(function(orders) {
    //  console.log(orders.data())
  
      document.getElementById("historyOrderList").innerHTML+=
      `<div class="card">`+
        `<div class="card-header">Sandwich: ${orders.data().ordername}</div>`+
        `<div class="card-content">`+
          ` <div class="list media-list no-ios-edges">`+
            `<ul>`+
              `<li class="item-content">`+
                `<div class="item-inner">`+
                    ` <div class="item-subtitle">Cleared: ${orders.data().completed}</div>`+
                    `</div>`+
                    ` </li>`+
                    ` <div class="item-content">${orders.data().orderavailable}</div>`+
                    ` <div class="item-content">Quantity: ${orders.data().quantity}</div>`+
                    ` <div class="item-content">Sauce: ${orders.data().ordersauce}</div>`+
                    `<div class="item-content">Extra's: ${orders.data().orderextra}</div>`+
                    `<div class="item-content">Customer: ${orders.data().customer}</div>`+
                    `<div class="item-content">Customer email: ${orders.data().customerMail}</div>`+
                    `</ul> </div></div>`+
                    `  <div class="card-footer"> <span>Date: ${orders.data().time.toDate()}</span><span> ${orders.data().orderTotalAmmountInEuro} euro</span></div>`+
                    ` </div>`

    })
  })
  


}

// rond een sandwich af.
function roundSandwich(orderID){


  var dbRound = db.collection("orders").doc(orderID);
  app.dialog.confirm("Is the sandwich really ready?", function(){

  // update document
    return dbRound.update({
      completed: true,
      orderRoundedAddress: "Vlezenbeek postweg "
   
    })
    .then(() => {
      app.dialog.alert("The sandwich has been successfully prepared!");
 
    })
    .catch((error) => {
        // The document probably doesn't exist.
        app.dialog.alert("This order does not excist !");
        
    });
  });

}


function getUserInformation() {

  firebase.auth().onAuthStateChanged(function (user) {

  if(firebase.auth().currentUser.uid != "byGAAV7hScOD8FJB4GtFWxGrnFz2" || firebase.auth().currentUser.uid === ""){
    $("#AdminBtn").remove();
          } 
         

    document.getElementById("userInfo").innerHTML+=`<div class="page-content">`+
    `<div class="card card-outline">`+
    ` <img src="${user.photoURL}" id="userImage" />`+
      `<div class="card-header" id="customerId">${user.displayName}</div>`+
      `<div class="card-content card-content-padding">${user.email}</div>`+
        ` <div class="card-footer">email verified: ${user.emailVerified}</div>`+
        ` </div>`+
        ` </div>`

  });

}



// toon alle orders op id
function getUserOrdersOnId(){
 
    db.collection("orders").where('customerID', '==', firebase.auth().currentUser.uid).orderBy("time", "desc").limit(10).onSnapshot(function(snapshot){
      document.getElementById("UserOrdersOnID").innerHTML='';
      document.getElementById("orderBadgeCount").innerHTML= snapshot.size;
      snapshot.forEach(function(orders) {
        
    
        document.getElementById("UserOrdersOnID").innerHTML+=
        `<div class="card">`+
          `<div class="card-header">Sandwich: ${orders.data().ordername}</div>`+
          `<div class="card-content">`+
            ` <div class="list media-list no-ios-edges">`+
              `<ul>`+
              `<li class="item-content">`+
              `<div class="item-inner">`+
                  ` <div class="item-subtitle">Done: ${orders.data().completed}</div>`+
                  `</div>`+
                  ` </li>`+
                  ` <div class="item-content">Quantity: ${orders.data().quantity}</div>`+
                  ` <div class="item-content">Sauce: ${orders.data().ordersauce}</div>`+
                  `<div class="item-content">Extra's: ${orders.data().orderextra}</div>`+
                  `<div class="item-content">Name Of Orderer: ${orders.data().customer}</div>`+
                  ` </ul> </div></div>`+
                  `  <div class="card-footer"> <span>Date: ${orders.data().time.toDate()}</span><span>${orders.data().orderTotalAmmountInEuro} euro</span></div>`+
                  `  <div class="card-footer"> <span style="color: green;">pick-up address: ${orders.data().orderRoundedAddress}</span></div>`+
                  ` </div>`
  
      })
    
    })
    
}
// toon alle orders op id wanneer de gebruiker op show more heeft gedrukt.
function getUserOrdersOnIdAll(){
 
  db.collection("orders").where('customerID', '==', firebase.auth().currentUser.uid).orderBy("time", "desc").onSnapshot(function(snapshot){
    document.getElementById("UserOrdersOnID").innerHTML='';
    document.getElementById("orderBadgeCount").innerHTML= snapshot.size;
    snapshot.forEach(function(orders) {
  
      document.getElementById("UserOrdersOnID").innerHTML+=
      `<div class="card">`+
        `<div class="card-header">Sandwich: ${orders.data().ordername}</div>`+
        `<div class="card-content">`+
          ` <div class="list media-list no-ios-edges">`+
            `<ul>`+
            `<li class="item-content">`+
            `<div class="item-inner">`+
                ` <div class="item-subtitle">Done: ${orders.data().completed}</div>`+
                `</div>`+
                ` </li>`+
                ` <div class="item-content">Quantity: ${orders.data().quantity}</div>`+
                ` <div class="item-content">Sauce: ${orders.data().ordersauce}</div>`+
                `<div class="item-content">Extra's: ${orders.data().orderextra}</div>`+
                `<div class="item-content">Name Of Orderer: ${orders.data().customer}</div>`+
                ` </ul> </div></div>`+
                `  <div class="card-footer"> <span>Date: ${orders.data().time.toDate()}</span><span>${orders.data().orderTotalAmmountInEuro} euro</span></div>`+
                `  <div class="card-footer"> <span style="color: green;">pick-up address: ${orders.data().orderRoundedAddress}</span></div>`+
                ` </div>`
    })
  
  })
  
}

/*function saveMessagingDeviceToken() {
  messaging.getToken().then(function(currentToken) {
    if (currentToken) {
      console.log('Got FCM device token:', currentToken);
      // Saving the Device Token to the datastore.
      db.collection('fcmTokens').doc(currentToken)
          .set({uid: firebase.auth().currentUser.uid});

          messaging.onMessage((payload) => console.log('Message received. ', payload));
    } else {
      // Need to request permissions to show notifications.
      sendNotification();
    }
  }).catch(function(error){
    console.error('Unable to get messaging token.', error);
  });


}

function sendNotification() {
 
    console.log('Requesting notifications permission...');
    messaging.requestPermission().then(function() {
      // Notification permission granted.
     saveMessagingDeviceToken();
    }).catch(function(error) {
      console.error('Unable to get permission to notify.', error);
    });
  
}*/





// het laden van meer broodjes wanneer de gebruiker om load more klikt.
function getOrdersAllOnLoadMore() {
  db.collection("orders").where('completed', '==', false).orderBy("time").onSnapshot(function(snapshot){
    document.getElementById("orderList").innerHTML='';
    document.getElementById("orderBadgeAdminCount").innerHTML= snapshot.size;
    snapshot.forEach(function(orders) {
    //  console.log(orders.data())
  
      document.getElementById("orderList").innerHTML+=
      `<div class="card">`+
        `<div class="card-header">Sandwich: ${orders.data().ordername}</div>`+
        `<div class="card-content">`+
          ` <div class="list media-list no-ios-edges">`+
            `<ul>`+
              `<li class="item-content">`+
                `<div class="item-inner">`+
                    ` <div class="item-subtitle">Ready: ${orders.data().completed}</div>`+
                    `</div>`+
                    ` </li>`+
                    ` <div class="item-content">${orders.data().orderavailable}</div>`+
                    ` <div class="item-content">Quantity: ${orders.data().quantity}</div>`+
                    ` <div class="item-content">Sauce: ${orders.data().ordersauce}</div>`+
                    `<div class="item-content">Extra's: ${orders.data().orderextra}</div>`+
                    `<div class="item-content">Customer: ${orders.data().customer}</div>`+
                    `<div class="item-content">Customer Email: ${orders.data().customerMail}</div>`+
                    ` </ul> </div></div>`+
                    `  <div class="card-footer"> <span>datum: ${orders.data().time.toDate()}</span><span>${orders.data().orderTotalAmmountInEuro} euro</span></div>`+
                    `<div class="button"><button onClick=roundSandwich('${orders.id}'); class='button button-fill button-raised button-small` +
                    `color-orange col'>Is Ready </button></div>`+
                    ` </div>`

    })
  })
}

// het laden van meer broodjes wanneer de gebruiker om load more klikt.
function getHistoryOrdersAllOnLoadMore() {
  db.collection("orders").where('completed', '==', true).orderBy("time", "desc").onSnapshot(function(snapshot){
    document.getElementById("historyOrderList").innerHTML='';
    snapshot.forEach(function(orders) {
    //  console.log(orders.data())
  
      document.getElementById("historyOrderList").innerHTML+=
      `<div class="card">`+
        `<div class="card-header">Sandwich: ${orders.data().ordername}</div>`+
        `<div class="card-content">`+
          ` <div class="list media-list no-ios-edges">`+
            `<ul>`+
              `<li class="item-content">`+
                `<div class="item-inner">`+
                    ` <div class="item-subtitle">cleared: ${orders.data().completed}</div>`+
                    `</div>`+
                    ` </li>`+
                    ` <div class="item-content">${orders.data().orderavailable}</div>`+
                    ` <div class="item-content">Quantity: ${orders.data().quantity}</div>`+
                    ` <div class="item-content">Sauce: ${orders.data().ordersauce}</div>`+
                    `<div class="item-content">Extra's: ${orders.data().orderextra}</div>`+
                    `<div class="item-content">Customer: ${orders.data().customer}</div>`+
                    `<div class="item-content">Customer Email: ${orders.data().customerMail}</div>`+
                    `</ul> </div></div>`+
                    `  <div class="card-footer"> <span>Time Of Order: ${orders.data().time.toDate()}</span><span>${orders.data().orderTotalAmmountInEuro} euro</span></div>`+
                    ` </div>`

    })
  })
}


function onDeviceReady() {
  signInCheck();
  signInUi();
  setTimeout(getTime, 1000);
  setTimeout(getSandwiches, 800);
  setTimeout(getSandwichesForUpdate, 800);
  setTimeout(getAllOrders, 2000);
 setTimeout(getAllOrdershistory, 2000);
  setTimeout(getUserOrdersOnId, 2000);
  setTimeout(getUserInformation, 2000)
  setTimeout(getSandwichesForDelete, 2500);

  $('#ToggleButton').on('click', function () {
      ToggleThemeButton();
     
  });

  $('#btnAddSandwich').on('click', function () {
  addSandwich();
  });

  $('#btnUpdateSandwich').on('click', function () {
    app.dialog.confirm("Are you sure?", function(){
    updateSandwiches();
  });
    });

      $('#LoadMoreOrderListBtn').on('click', function () {
        getOrdersAllOnLoadMore();
        $("#LoadMoreOrderListBtn").remove();
        });

        $('#LoadMoreHistoryBtn').on('click', function () {
          getHistoryOrdersAllOnLoadMore();
          $("#LoadMoreHistoryBtn").remove();
          });

          $('#LoadMoreBtn').on('click', function () {
            getUserOrdersOnIdAll();
            $("#LoadMoreBtn").remove();
            });
}